{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a46ba2ea-da54-4cb0-aaad-141ada21bd0d",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Configuring the cluster\n",
    "\n",
    "This notebook explains the procedure that has to be taken to configure specific nodes with a user configuration prior to computing.\n",
    "The bitstreams and device tree overlays have to be generated using Vivado and XCST tools.\n",
    "The process is detailed in [hyperFPGA gitlab project](https://gitlab.com/ictp-mlab/hyperfpga-linux).\n",
    "\n",
    "There is a convention all configuration files must follow:\n",
    "\n",
    "* All files corresponding to the same project must have the same ```filename```.\n",
    "* All files must include the targeted FPGA model in the name, if targetting different FPGAs all files must be present.\n",
    "* All files must be uploaded to the ```/bitstreams``` folder in the your user's account.\n",
    "* Filename: ```<project name>-<fpga model>.<bin or dtbo>```\n",
    "\n",
    "> WARNING: Filename cannot be changed once the ```xsa``` file was generated. If you want to change the name of the bitstream, you have to export again the ```xsa```.\n",
    "\n",
    "Example:\n",
    "\n",
    "Project: test\n",
    "```\n",
    "/bistreams/test-4ge21.bin\n",
    "/bistreams/test-4ge21.dtbo\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "e85c439a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import asyncio, time, asyncssh\n",
    "from pathlib import Path"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "019f29cb-17ff-4951-ad94-adadfc28a961",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Load nodes config file into a Python dictionary\n",
    "\n",
    "All nodes are defined inside a JSON file following this structure:\n",
    "\n",
    "```json\n",
    "{\n",
    "    \"hostname\":\"hyperfpga-4ge21-1-1\",\n",
    "    \"ip\":\"192.168.0.7\",\n",
    "    \"x\":1,\n",
    "    \"y\":1,\n",
    "    \"fpga\":{\n",
    "        \"model\":\"4ge21\",\n",
    "        \"state\":\"unknown\",\n",
    "        \"firmware\":\"\"\n",
    "    },\n",
    "    \"comblock\":{\n",
    "        \"devs\":[]\n",
    "    }\n",
    "}\n",
    "```\n",
    "\n",
    "All entries are required but for the moment not all are used. \n",
    "This file should be generated automatically for each user depending on the nodes available for use specified by the administrator via Jupyter.\n",
    "\n",
    "An example of how the file is loaded and specific entries are recovered is showed here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "534415d2-d3ef-497d-a98d-35bb8a49410c",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'192.168.0.7'"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import json\n",
    "\n",
    "with open('nodes.json') as nodes_file:\n",
    "    nodes = json.load(nodes_file)['nodes']\n",
    "\n",
    "nodes[0]['ip']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1d013632-c3a0-4042-9a11-11ae9bef6415",
   "metadata": {},
   "source": [
    "## SCP Send via asyncssh\n",
    "\n",
    "To automate the distribution of the bitstreams and device tree overlays we rely on scp calls wrapped in custom Python code.\n",
    "This way anyone can craft their own loader depending on the strategy.\n",
    "The following steps must be followed:\n",
    "\n",
    "1. Upload ```.bin``` and ```.dtbo``` files to the ```~/bitstreams``` folder of your user's account.\n",
    "    > Note: both files must have the same name as both are required to properly configure the FPGA and Linux.\n",
    "2. Create your custom loader, or used the one provided below as an example.\n",
    "3. All files must be copied into ```/tmp/``` folder of the targeted nodes to avoid filling up space.\n",
    "\n",
    "The function ```upload_firmware()``` provides a boiler plate that shows how to use ```asyncssh.scp``` to send the configuration files.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "2e5ac6ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "start_time = 0\n",
    "\n",
    "def progress_handler(from_file, to_file, bytes_copied, total_size):\n",
    "    time_spent = time.perf_counter() - start_time\n",
    "    print(f\"{from_file} -> {to_file} {bytes_copied/time_spent:06.2f}\", end='\\r')\n",
    "\n",
    "async def scp_handler(host: str, filename: str):\n",
    "    try:\n",
    "        async with asyncssh.connect(host, username='mlabadm') as conn:\n",
    "            await asyncssh.scp(filename, (conn, '/tmp/'), progress_handler = progress_handler)\n",
    "            return 1\n",
    "    except (OSError, asyncssh.Error) as exc:\n",
    "        print(f'Error: SSH connection failed {str(exc)}')\n",
    "        return 0\n",
    "\n",
    "async def upload_firmware(destination_nodes: list, filename: str):\n",
    "    # check that the destination nodes have valid ips\n",
    "    # collect targetted fpgas to validate the files\n",
    "    for node in destination_nodes:\n",
    "        if not( 'ip' in node.keys()):\n",
    "            print(f'Error: missing ip in node {node}')\n",
    "            return\n",
    "        if not('fpga' in node.keys()):\n",
    "            print(f'Error: missing fpga in node {node}')\n",
    "            return\n",
    "        # check that the both filename.bin and filename.dtsi files are available is present in ~/bitstreams and both \n",
    "        bit  = Path(f\"~/bitstreams/{filename}-{node['fpga']['model']}.bit.bin\").expanduser()\n",
    "        dtbo  = Path(f\"~/bitstreams/{filename}-{node['fpga']['model']}.dtbo\").expanduser()\n",
    "        if not (bit.is_file() and dtbo.is_file()):\n",
    "            print(f\"Error: missing configuration files for FPGA {node['fpga']['model']}\")\n",
    "            return\n",
    "        if not await scp_handler(node['ip'], bit):\n",
    "            print(f\"Error: failed to copy {bit} to {node['ip']}.\")\n",
    "            return\n",
    "        if not await scp_handler(node['ip'], dtbo):\n",
    "            print(f\"Error: failed to copy {dtbo} to {node['ip']}.\")\n",
    "            return"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "4cd22cf3-25fb-4fbc-b5ed-806836e4f40d",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "b'/home/jupyter-mlabadm/bitstreams/hyperFPGA-bdf-test-3be11.dtbo' -> b'' 000.00.30\r"
     ]
    }
   ],
   "source": [
    "await upload_firmware(nodes, 'hyperFPGA-bdf-test')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab410335-c343-4108-8ba3-2db9fc0af7e2",
   "metadata": {},
   "source": [
    "## Configuring the FPGAs\n",
    "\n",
    "Now that the configuration files are copied into the boards the next step is finding out how to apply the specified configuration. \n",
    "Xilinx provides a nice ```C``` wrapper by the name of ```fpgautil``` that deals with the **FPGA** device in Linux.\n",
    "We just need to check that we have sufficient permissions to use it and validate that the process was completed successfully."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "210394f3-e711-4db0-8dea-ce6c019df322",
   "metadata": {},
   "outputs": [],
   "source": [
    "async def fpgautil_handler(host: str, firmware: str):\n",
    "    try:\n",
    "        async with asyncssh.connect(host, username='mlabadm') as conn:\n",
    "            result = await conn.run(f'sudo fpgautil -b /tmp/{firmware}.bit.bin -o /tmp/{firmware}.dtbo\\n')\n",
    "            print(result.stdout, end= '')\n",
    "            return 1\n",
    "    except (OSError, asyncssh.Error) as exc:\n",
    "        print(f'Error: SSH connection failed {str(exc)}')\n",
    "        return 0\n",
    "\n",
    "async def program_fpgas(destination_nodes: list, filename: str):\n",
    "    # check that the destination nodes have valid ips\n",
    "    # collect targetted fpgas to validate the files\n",
    "    for node in destination_nodes:\n",
    "        if not( 'ip' in node.keys()):\n",
    "            print(f'Error: missing ip in node {node}')\n",
    "            return\n",
    "        if not('fpga' in node.keys()):\n",
    "            print(f'Error: missing fpga in node {node}')\n",
    "            return\n",
    "        firmware = f\"{filename}-{node['fpga']['model']}\"\n",
    "        if not await fpgautil_handler(node['ip'], firmware):\n",
    "            print(f\"Error: failed to program {node['ip']}.\")\n",
    "            return\n",
    "        else:\n",
    "            node['fpga']['firmware'] = firmware"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "5b314fe7-992f-4641-9c05-d685e651bb41",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Time taken to load BIN is 169.000000 Milli Seconds\n",
      "BIN FILE loaded through FPGA manager successfully\n",
      "Time taken to load BIN is 170.000000 Milli Seconds\n",
      "BIN FILE loaded through FPGA manager successfully\n",
      "Time taken to load BIN is 172.000000 Milli Seconds\n",
      "BIN FILE loaded through FPGA manager successfully\n",
      "Time taken to load BIN is 163.000000 Milli Seconds\n",
      "BIN FILE loaded through FPGA manager successfully\n",
      "Time taken to load BIN is 168.000000 Milli Seconds\n",
      "BIN FILE loaded through FPGA manager successfully\n",
      "Time taken to load BIN is 171.000000 Milli Seconds\n",
      "BIN FILE loaded through FPGA manager successfully\n",
      "Time taken to load BIN is 164.000000 Milli Seconds\n",
      "BIN FILE loaded through FPGA manager successfully\n",
      "Time taken to load BIN is 167.000000 Milli Seconds\n",
      "BIN FILE loaded through FPGA manager successfully\n",
      "Time taken to load BIN is 163.000000 Milli Seconds\n",
      "BIN FILE loaded through FPGA manager successfully\n",
      "\r"
     ]
    }
   ],
   "source": [
    "await program_fpgas(nodes, 'hyperFPGA-bdf-test')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b32ecd65-e411-4a12-b258-b4c45de406c3",
   "metadata": {},
   "source": [
    "## Read the state of the FPGAs\n",
    "\n",
    "The fpga_manager class in Linux provides two ways of determining the state of the FPGA.\n",
    "The first one is the most simple which consists of retrieving the reported state of the FPGA via the ```/sys/class/fpga_manager/fpga0/state``` file.\n",
    "In case a more detail descritption of the device is desired, first the debugging option must be enabled in the kernel options. \n",
    "\n",
    "### Read FPGA configuration registers\n",
    "\n",
    "1. In petalinux open the kernel configuration window:\n",
    "    ```petalinux-confing -c kernerl```\n",
    "    \n",
    "2. Go to ```Device Drivers -> FPGA Configuration Framework```\n",
    "\n",
    "3. Enable ```FPGA debug fs```\n",
    "\n",
    "Once this is done and the kernel is compiled and booted, the configuration registers of the FPGA can be accessed by reading the file ```/sys/kernel/debug/fpga/fpga0/image``` or using the ```fpgautil -r``` command.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "4243b97c-c505-4b95-b81b-241c00027625",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "async def read_state(host: str):\n",
    "    try:\n",
    "        async with asyncssh.connect(host, username='mlabadm') as conn:\n",
    "            result = await conn.run(f'sudo cat /sys/class/fpga_manager/fpga0/state')\n",
    "            return result.stdout\n",
    "    except (OSError, asyncssh.Error) as exc:\n",
    "        print(f'Error: SSH connection failed {str(exc)}')\n",
    "        return 0\n",
    "\n",
    "async def read_fpgas_state(destination_nodes: list):\n",
    "    # check that the destination nodes have valid ips\n",
    "    # collect targetted fpgas to validate the files\n",
    "    for node in destination_nodes:\n",
    "        if not( 'ip' in node.keys()):\n",
    "            print(f'Error: missing ip in node {node}')\n",
    "            return\n",
    "        if not('fpga' in node.keys()):\n",
    "            print(f'Error: missing fpga in node {node}')\n",
    "            return\n",
    "        result = await read_state(node['ip'])\n",
    "        if not result:\n",
    "            break\n",
    "        else:\n",
    "            node['fpga']['state'] = result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "d6a5d992-3e93-413e-9d15-0a50be07c9cf",
   "metadata": {},
   "outputs": [],
   "source": [
    "await read_fpgas_state(nodes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "4dcb12e8-0087-4974-95b0-848b5eff02cf",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[{'hostname': 'hyperfpga-4ge21-1-1',\n",
       "  'ip': '192.168.0.7',\n",
       "  'x': 1,\n",
       "  'y': 1,\n",
       "  'fpga': {'model': '4ge21',\n",
       "   'state': 'operating\\n',\n",
       "   'firmware': 'hyperFPGA-bdf-test-4ge21'},\n",
       "  'comblock': {'devs': []}},\n",
       " {'hostname': 'hyperfpga-4ge21-1-2',\n",
       "  'ip': '192.168.0.8',\n",
       "  'x': 1,\n",
       "  'y': 2,\n",
       "  'fpga': {'model': '4ge21',\n",
       "   'state': 'operating\\n',\n",
       "   'firmware': 'hyperFPGA-bdf-test-4ge21'},\n",
       "  'comblock': {'devs': []}},\n",
       " {'hostname': 'hyperfpga-4ge21-1-3',\n",
       "  'ip': '192.168.0.9',\n",
       "  'x': 1,\n",
       "  'y': 3,\n",
       "  'fpga': {'model': '4ge21',\n",
       "   'state': 'operating\\n',\n",
       "   'firmware': 'hyperFPGA-bdf-test-4ge21'},\n",
       "  'comblock': {'devs': []}},\n",
       " {'hostname': 'hyperfpga-3be11-2-1',\n",
       "  'ip': '192.168.0.11',\n",
       "  'x': 2,\n",
       "  'y': 1,\n",
       "  'fpga': {'model': '3be11',\n",
       "   'state': 'operating\\n',\n",
       "   'firmware': 'hyperFPGA-bdf-test-3be11'},\n",
       "  'comblock': {'devs': []}},\n",
       " {'hostname': 'hyperfpga-3be11-2-2',\n",
       "  'ip': '192.168.0.12',\n",
       "  'x': 2,\n",
       "  'y': 2,\n",
       "  'fpga': {'model': '3be11',\n",
       "   'state': 'operating\\n',\n",
       "   'firmware': 'hyperFPGA-bdf-test-3be11'},\n",
       "  'comblock': {'devs': []}},\n",
       " {'hostname': 'hyperfpga-4ge21-2-3',\n",
       "  'ip': '192.168.0.13',\n",
       "  'x': 2,\n",
       "  'y': 3,\n",
       "  'fpga': {'model': '4ge21',\n",
       "   'state': 'operating\\n',\n",
       "   'firmware': 'hyperFPGA-bdf-test-4ge21'},\n",
       "  'comblock': {'devs': []}},\n",
       " {'hostname': 'hyperfpga-3be11-3-1',\n",
       "  'ip': '192.168.0.15',\n",
       "  'x': 3,\n",
       "  'y': 1,\n",
       "  'fpga': {'model': '3be11',\n",
       "   'state': 'operating\\n',\n",
       "   'firmware': 'hyperFPGA-bdf-test-3be11'},\n",
       "  'comblock': {'devs': []}},\n",
       " {'hostname': 'hyperfpga-3be11-3-2',\n",
       "  'ip': '192.168.0.16',\n",
       "  'x': 3,\n",
       "  'y': 2,\n",
       "  'fpga': {'model': '3be11',\n",
       "   'state': 'operating\\n',\n",
       "   'firmware': 'hyperFPGA-bdf-test-3be11'},\n",
       "  'comblock': {'devs': []}},\n",
       " {'hostname': 'hyperfpga-3be11-3-3',\n",
       "  'ip': '192.168.0.17',\n",
       "  'x': 3,\n",
       "  'y': 3,\n",
       "  'fpga': {'model': '3be11',\n",
       "   'state': 'operating\\n',\n",
       "   'firmware': 'hyperFPGA-bdf-test-3be11'},\n",
       "  'comblock': {'devs': []}}]"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "nodes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8b4eb93-f004-4e3d-92f1-6869817eb7d7",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "python3.11-kernel",
   "language": "python",
   "name": "python3.11-kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
