# Standard library imports
import time
import json
import os
import re
from re import findall
from subprocess import Popen, PIPE, run, CalledProcessError
from pathlib import Path
from warnings import warn
from typing import Union

# Third-party imports
import pkg_resources
import yaml
import asyncssh
from ipyparallel import Cluster
from PIL import Image, ImageDraw, ImageFont
from IPython.display import display, clear_output


class HyperFPGACluster(Cluster):
    
    def __init__(self,
                 nodes: dict = None,
                 firmware: Union[str, list] = None,
                 n_engines: int = 1,
                 engines_per_node: int = 1,
                 profile: str = 'ssh',
                 bitstream_path: str = 'bitstreams'
    ) -> None:
        """
        Initialize the HyperFPGACluster object.

        Parameters
        ----------
        nodes : dict, optional
            A dictionary of nodes to be used in the cluster. If not provided, the
            nodes specified in the configuration file will be used.
        firmware : str or list, optional
            The firmware to be used on the FPGAs. If not provided, the firmware
            specified in the configuration file will be used.
        n_engines : int, optional
            The total number of engines to start. If not provided, the number of
            engines per node is used to calculate the total number of engines.
        engines_per_node : int, optional
            The number of engines to start on each node. Defaults to 1.
        profile : str, optional
            The profile to use when starting the cluster. Defaults to 'ssh'.
        """
        self.work_dir = Path.cwd()
        self.bitstream_path = bitstream_path
        self.nodes = nodes
        self.engines_per_node = engines_per_node
        self.profile = profile
        self.n_engines = n_engines
        self.__load_config()
        self.firmware = firmware
    
    @property
    def bitstream_path(self):
        return self._bitstream_path

    @bitstream_path.setter
    def bitstream_path(self, bitstream_path):
        if isinstance(bitstream_path, str):
            bitstream_path = self.work_dir / Path(bitstream_path)
            if not bitstream_path.is_dir():
                bitstream_path.mkdir(parents=True, exist_ok=True)
            self._bitstream_path = bitstream_path
        else:
            raise TypeError("Bitstream path must be a string.")
        
    @property
    def nodes(self):
        return self._nodes

    @nodes.setter
    def nodes(self, nodes):
        if isinstance(nodes, list):
            self._nodes = nodes
        elif isinstance(nodes, dict):
            self._nodes = [nodes]
        else:
            raise TypeError("Nodes must be a list or a dict.")

    @property
    def profile(self):
        return self._profile

    @profile.setter
    def profile(self, profile):
        if isinstance(profile, str):
            self._profile = profile
        else:
            raise TypeError("Profile name must be a string.")

    @property
    def engines_per_node(self):
        return self._engines_per_node

    @engines_per_node.setter
    def engines_per_node(self, engines_per_node):
        if isinstance(engines_per_node, int) and engines_per_node > 0 and engines_per_node <= 4:
            self._engines_per_node = engines_per_node
        else:
            raise TypeError(f"Engines per node must be an integer bigger than 0 and less than 4.")

    @property
    def n_engines(self):
        return self.n_engines

    @n_engines.setter
    def n_engines(self, n_engines):
        if isinstance(n_engines, int) and n_engines > 0 and n_engines <= len(self._nodes) * self._engines_per_node:
            self._n_engines = n_engines
        else:
            raise TypeError(f"Engines must be an integer bigger than 0 and less than {len(self._nodes) * self._engines_per_node}.")

    def __load_config(self):
        """
        Load the configuration from the config.yml file.

        The configuration file is assumed to be in the same package as this module.
        The following configuration parameters are loaded:

        - CONTROLLER_IP: The IP address of the controller node.
        - NODES_PATH: The path to the directory containing the node configuration files.
        - NODE_USER_NAME: The username to use when connecting to the nodes.
        - IMAGE_PATH: The path to the image file to use for the nodes.
        - XSA_2_BIT_PATH: The path to the xsa2bin tool.
        - VIVADO_PATH: The path to the Vivado tool.
        - DTS_PATH: The path to the device tree source files.
        - BITSTREAM_PATH: The path to the bitstream files.
        - SSH_KEY_PATH: The path to the SSH key to use for connecting to the nodes.
        """
        with pkg_resources.resource_stream(__name__, 'assets/config.yml') as config_file:
            config = yaml.safe_load(config_file.read())

        self.CONTROLLER_IP = config['controller']['host']
        self.NODES_PATH = config['nodes']['path']
        self.NODE_USER_NAME = config['nodes']['user']
        self.XSA_2_BIT_PATH = config['tools']['xsa2bin']
        self.VIVADO_PATH = config['tools']['vivado']
        self.DTS_PATH = config['tools']['dts']
        self.SSH_KEY_PATH = config['settings']['ssh_key_path']
        
        #self.IMAGE_PATH = config['image']['path']

    @property
    def firmware(self):
        return self._firmware
          
    @firmware.setter
    def firmware(self, firmware):
        def verify_files(firmware, index):
            bit, dtbo, xsafile = self.__check_config_files(firmware, index)
            if not bit.is_file() and not dtbo.is_file():
                if not xsafile.is_file():
                    raise ValueError(f"Error: Missing configuration files {xsafile} for FPGA in bitstreams folder.")
                print("Warning: Missing .bit and .dtbo files. They will be created, which may take a few minutes.")
                self.__exec_xsa2bin(xsafile, bit)
            else:
                if not bit.is_file():
                    raise ValueError(f"Error: Missing configuration file {bit} for FPGA in bitstreams folder.")
                if not dtbo.is_file():
                    raise ValueError(f"Error: Missing configuration file {dtbo} for FPGA in bitstreams folder.")

        if isinstance(firmware, str):
            verify_files(firmware, 0)
            self._firmware = [firmware] * len(self._nodes)
    
        elif isinstance(firmware, list):
            if len(firmware) != len(self._nodes):
                raise ValueError(f"Error: Configuration mismatch. Expected {len(self._nodes)} firmware files, got {len(firmware)}.")
    
            for i, f in enumerate(firmware):
                verify_files(f, i)
    
            self._firmware = firmware
            
    def __check_config_files(self, firmware, i):
        xsafile = self.work_dir / f"{firmware}-{self._nodes[i]['fpga']['model']}.xsa"
        path_fpga = self.bitstream_path / f"{firmware}-{self._nodes[i]['fpga']['model']}"
        bit = path_fpga.with_suffix(".bit.bin")
        dtbo = path_fpga.with_suffix(".dtbo")
    
        return bit, dtbo, xsafile
            
    def __exec_xsa2bin(self, xsafile, bit):
        try:
            cm_exec = [
                "python3", self.XSA_2_BIT_PATH,
                f"-x={xsafile}", f"-V={self.VIVADO_PATH}",
                f"-dts={self.DTS_PATH}"
            ]

            run(cm_exec, check=True, stdout=PIPE, stderr=PIPE, cwd=self.bitstream_path)
            print(f".bit, .dtbo and .dtsi files were created from {xsafile}")

        except CalledProcessError as e:
            raise RuntimeError(f"XSA to BIT conversion failed: {e.stderr.decode()}") from e
        
    async def __scp_handler(self, host: str, filename: str):
        try:
            async with asyncssh.connect(host, username = self.NODE_USER_NAME, client_keys = self.SSH_KEY_PATH) as conn:
                await asyncssh.scp(filename, (conn, '/tmp/'), progress_handler = self.__progress_handler)
                return True
        except(OSError, asyncssh.Error) as exc:
            warn(f'Error: SSH connection to host {host} failed. {str(exc)}', RuntimeWarning)
            return False

    async def __sshcmd(self, host, cmd: str):
        try:
            async with asyncssh.connect(host, username = self.NODE_USER_NAME, client_keys = self.SSH_KEY_PATH) as conn:
                result = await conn.run(f'sudo {cmd}')
                return result.stdout
        except(OSError, asyncssh.Error) as exc:
            warn(f'Error: SSH connection to host {host} failed. {str(exc)}.', RuntimeWarning)
            return 0
        
    def __progress_handler(self, from_file, to_file, bytes_copied, total_size):
        time_spent = time.perf_counter()
        print(f"{from_file} -> {to_file} {bytes_copied/time_spent:06.2f}", end='\r')
    
    def __ping (self, host, ping_count = 4):
        data = ""
        output = Popen(f"ping {host} -n {ping_count}", stdout=PIPE, encoding="utf-8")
        for line in output.stdout:
            if findall("TTL", line):
                return 1
            else:
                return 0
    
    async def __load_driver(self, driver : str = 'comblock'):
        # check that the destination nodes have valid ips
        # collect targetted fpgas to validate the files
        for node in self._nodes:
            if not( 'ip' in node):
                warn(f'Error: missing ip in node {node}', RuntimeWarning)
                return
            if not('fpga' in node):
                warn(f'Error: missing fpga in node {node}', RuntimeWarning)
                return
            await self.__sshcmd(node['ip'], f'sudo insmod {driver}.ko')
    
    async def __upload_firmware(self):
        # check that the destination nodes have valid ips
        # collect targetted fpgas to validate the files
        for node, firmware in zip(self._nodes, self._firmware):
            if not('ip' in node.keys()):
                warn(f'Error: missing ip in node {node}', RuntimeWarning)
                return
            if not('fpga' in node.keys()):
                warn(f'Error: missing fpga in node {node}', RuntimeWarning)
                return
            # check that the both filename.bin and filename.dtsi files are available is present in ~/bitstreams and both
            bit  = self.bitstream_path / f"{firmware}-{node['fpga']['model']}.bit.bin"
            dtbo  = self.bitstream_path / f"{firmware}-{node['fpga']['model']}.dtbo"
            if not (bit.is_file() and dtbo.is_file()):
                warn(f"Error: missing configuration files for FPGA {node['fpga']['model']}", RuntimeWarning)
                return
            if not await self.__scp_handler(node['ip'], bit):
                warn(f"Error: failed to copy {bit} to {node['ip']}.", RuntimeWarning)
                return
            if not await self.__scp_handler(node['ip'], dtbo):
                warn(f"Error: failed to copy {dtbo} to {node['ip']}.", RuntimeWarning)
                return
        
    async def __fpgautil_handler(self, host: str, firmware: str =  None):
        try:       
            if firmware:
                op = f"-b /tmp/{firmware}.bit.bin -o /tmp/{firmware}.dtbo"
            else:
                op = "-R"
            result = await self.__sshcmd(host, f'sudo fpgautil {op}')
            print(result, end = '')
            return 1
        except (OSError, asyncssh.Error) as exc:
            warn(f'Error: SSH connection failed {str(exc)}', RuntimeWarning)
            return 0

    async def __program_fpgas(self):
        # check that the destination nodes have valid ips
        # collect targetted fpgas to validate the files
        for node, firmware in zip(self._nodes, self._firmware):
            if not( 'ip' in node.keys()):
                warn(f'Error: missing ip in node {node}', RuntimeWarning)
                return
            if not('fpga' in node.keys()):
                warn(f'Error: missing fpga in node {node}', RuntimeWarning)
                return
            if not await self.__fpgautil_handler(node['ip'], f"{firmware}-{node['fpga']['model']}"):
                warn(f"Error: failed to program {node['ip']}.", RuntimeWarning)
                return
            else:
                node['fpga']['firmware'] = firmware

    async def __fpga_state(self, host: str):
        try:
            async with asyncssh.connect(host, username=self.NODE_USER_NAME, client_keys=self.SSH_KEY_PATH) as conn:
                result = await conn.run(f'sudo cat /sys/class/fpga_manager/fpga0/state')
                return result.stdout
        except (OSError, asyncssh.Error) as exc:
            warn(f'Error: SSH connection failed {str(exc)}', RuntimeWarning)
            return 0
        
    async def __read_state(self):
        # check that the destination nodes have valid ips
        # collect targetted fpgas to validate the files
        for node in self._nodes:
            if not( 'ip' in node.keys()):
                warn(f'Error: missing ip in node {node}', RuntimeWarning)
                return
            if not('fpga' in node.keys()):
                warn(f'Error: missing fpga in node {node}', RuntimeWarning)
                return
            result = await self.__fpga_state(node['ip'])          
            if not result:
                break
            else:
                node['fpga']['state'] = result

            result = await self.__sshcmd(node['ip'], 'ls /dev/ComBlock*')
            node['comblock']['devs'] =  re.findall(r'_\d+_([a-z_]+)', result)
        
    def __ipy_profile(self, hosts, engine_args: str, controller_ip: str) -> str :
        return f"""
c = get_config()
c.Cluster.controller_ip = '{controller_ip}'
c.Cluster.engine_launcher_class = 'ssh'
c.SSHEngineSetLauncher.engine_args = {engine_args}
c.SSHEngineSetLauncher.engines = {hosts}
"""
    
    # Cluster methods
    async def clean_cluster(self):
        for node in self._nodes:
            await self.__sshcmd(node['ip'], f'sudo rmmod comblock')
            await self.__fpgautil_handler(node['ip'])
            node['fpga']['firmware'] = ''

    async def reboot_cluster(self):
        for node in self._nodes:
            await self.__sshcmd(node['ip'], 'sudo reboot')

    async def shutdown_cluster(self):
        for node in self._nodes:
            await self.__sshcmd(node['ip'], 'sudo shutdown')
            
    async def configure(self, view=False):
        await self.clean_cluster()
        print('Uploading firmware...')
        await self.__upload_firmware()
        print('\n Programming fpgas...')
        await self.__program_fpgas()
        print('\n Node Status')
        await self.__load_driver()
        if view:
            node_view(self._nodes, programed=True)
        await self.print_state()
        super().__init__(profile = self._profile, n = self._n_engines)
        
    def create_profile(self, mpi: bool = False) -> dict :
        username = os.environ.get("USER")
        if mpi:
            warn("Check that MPI is available in the nodes.", RuntimeWarning)
            engine_args = ["--engines=mpi", f"--profile-dir=/home/mlabadm/.ipython/profile_{self._profile}"]
        else: 
            engine_args = [f"--profile-dir=/home/mlabadm/.ipython/profile_{self._profile}"]
        profile = self.__ipy_profile({f'{self.NODE_USER_NAME}@{node["ip"]}': self._engines_per_node for node in self._nodes}, engine_args, self.CONTROLLER_IP)
        profile_folder = f"/home/{username}/.ipython/profile_{self._profile}"
        if os.path.isdir(profile_folder):
            print("Profile exists, rewritting configuration.")
        else:
            print("Profile does not exist, creating new profile.")
            os.mkdir(profile_folder)
        with open(profile_folder + "/ipcluster_config.py", 'w') as file:
            file.write(profile)
        
    async def print_state(self):
        await self.__read_state()
        for node in self._nodes:
            hostname = node['hostname']
            firmware = node['fpga']['firmware']
            state = node['fpga']['state']
            comblock = node['comblock']['devs']
            print(f' host: {hostname} state: {state} firmware: {firmware} comblock: {comblock}\n')
            
def get_nodes(
    node_path: str = "/srv/data/hyperfpga_conf/.nodes_assig/",
    view: bool = False
) -> list:
    user = os.environ.get("USER")
    nodesFile = f"{node_path}{user}_nodes.json"
    with open(nodesFile) as nodes_file:
        nodes = json.load(nodes_file)['nodes']
    node_names = [node['hostname'] for node in nodes]
    
    if view:
        node_view(nodes)
    else:
        print(f"Assigned Nodes: {node_names}")
    return nodes
    
def node_view(nodes, programed = False, square_size=123):
    def draw_node_rect(draw, x_pos, y_pos, square_size, edgecolor):
        draw.rectangle([y_pos, x_pos, y_pos + square_size, x_pos + square_size],
                       outline=edgecolor, width=3, fill="white")

    def add_node_text(draw, x_pos, y_pos, square_size, text, color):
        center_x = x_pos + square_size / 2
        center_y = y_pos + square_size / 2
        #font = ImageFont.truetype("arial.ttf", size=12) 
        font = ImageFont.load_default(size=14) 
        bbox = draw.textbbox((0, 0), text, font=font)
        text_width = bbox[2] - bbox[0]
        text_height = bbox[3] - bbox[1]
        draw.text((center_y - text_width / 2, center_x - text_height / 2),
                  text, fill=color, font=font)

    with pkg_resources.resource_stream(__name__, 'assets/hyperfpga.jpg') as img_file:
        image_file = Image.open(img_file)
        image = image_file.copy()
        image_file.close()
        
    draw = ImageDraw.Draw(image)
    rect_color = 'blue' if programed else 'red'
    text_color = 'red'
    
    for i, node in enumerate(nodes):
        x, y, ip, model = node['x'], node['y'], node['ip'], node['fpga']['model']
        hostname = f"{model}-{x}-{y}"
        x_position, y_position = x * square_size, y * square_size
        draw_node_rect(draw, x_position, y_position, square_size, rect_color)
        if programed:
            node_text = f'hostname: \n {hostname}'
        else:
            node_text = f'node: {i} \n model: {model}\n ip: {ip}'
        
        add_node_text(draw, x_position, y_position, square_size, node_text, text_color)
    
    display(image)