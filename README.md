# HyperFPGA Software

This repository holds the examples and resources which might prove useful to anyone approaching the HyperFPGA.
All examples are built around JupyterLab, considering how simple it is to build and tests apps in its interactive environment.
Nonetheless, the same apps can be converted in standalone Python apps to run outside the Jupyter framework.

At the same time, a library is provided that wraps the configuration and control of the FPGA extending the capabilities of IPyParallel.
This is a work in progress and will greatly benefit from feedback.

## Examples

Four examples are provided under the ```examples/``` folder.
These consist of a series of notebooks that layout the configuration and interaction mechanisms of the HyperFPGA and IPyParallel.

## clusterconf

This Python library provides a unified class that encompasses all configuration tasks.

> Documentation under construction!!
